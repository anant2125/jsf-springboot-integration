# JSF-SpringBoot-Integration

A POC integrating JSF2.0 framework with Springboot2


Technology:

1. JSF2.0
2. Myfaces2.2.12
3. Rewrite 3.4
4. Primefaces6.1
5. SpringBoot 2.3.2
6. SpringDataJPA
7. JDBC


Its a POC project for integration of JSF with SpringBoot. It includes JPA implementation with SpringBoot and MS SQL Server, Can be replaced with any ORM/Database.
Its a Maven build project.
Rewrite is used to beautify URLs and removing URL complexities. 


Software Requiremens:

1. Maven 
2. Java8 
3. Eclipse(Any IDE supportingSpringBoot & Lombok)
4. MS SQL Server




Operation:
1. Clone the repository.
2. Import the poc porject as a Existing Maven project
3. Update Maven Dependencies.
4. Change Database configuration in application.properties file.
5. Run as SpringBoot Project (Can be build with Maven "mvn clean install"  and war file at/output folder can be deployed on Tomcat Server Directly)
6. Access the JSF application on browser with http://localhost:8080/
7. Access RestAPI with http://localhost:8080/rest/greet/{name}



Reference :https://dzone.com/articles/developing-jsf-applications-with-spring-boot